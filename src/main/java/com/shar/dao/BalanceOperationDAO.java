package com.shar.dao;

import com.shar.model.BalanceOperation;
import java.util.List;

public interface BalanceOperationDAO {

    List<BalanceOperation> listBalanceOperations();

    void saveBalanceOperation(BalanceOperation balanceOperation);

    void deleteBalanceOperation(int id);

    BalanceOperation getBalanceOperation(int id);
}
