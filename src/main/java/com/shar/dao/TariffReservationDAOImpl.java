package com.shar.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.shar.model.TariffReservation;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Repository("TariffReservationDAO")
public class TariffReservationDAOImpl implements TariffReservationDAO {

    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public void saveTariffReservation(TariffReservation tariffReservation) {
        sessionFactory.getCurrentSession().saveOrUpdate(tariffReservation);
    }
    
    @Override
    public TariffReservation getTariffReservation(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from TariffReservation where id = :id");
        query.setLong("id", id);
        return (TariffReservation) query.uniqueResult();
    }
    
    @Override
    public TariffReservation getActiveTariffReservation(){
        Query query = sessionFactory.getCurrentSession().createQuery("from TariffReservation where Status = 'Active'");
        return (TariffReservation) query.uniqueResult();
    }
    
    @Override
    public List<TariffReservation> getDisableTariffReservation(){
        try {
            return sessionFactory.getCurrentSession().createQuery("from TariffReservation where Status <> 'Active'").list();
        } catch (Exception ex) {
            throw new RuntimeException("Can't get from DB", ex);
        }
    }       
}
