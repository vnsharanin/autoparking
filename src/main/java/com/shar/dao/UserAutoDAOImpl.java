package com.shar.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.shar.model.UserAuto;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Repository("UserAutoDAO")
public class UserAutoDAOImpl implements UserAutoDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<UserAuto> listUserAutos() {
        try {
            return sessionFactory.getCurrentSession().createQuery("from UserAuto").list();
        } catch (Exception ex) {
            throw new RuntimeException("Can't get from DB", ex);
        }
    }

    public void saveUserAuto(UserAuto userAuto) {
        sessionFactory.getCurrentSession().saveOrUpdate(userAuto);
    }

    @Override
    public void deleteUserAuto(int id) {
        UserAuto userAuto = (UserAuto) sessionFactory.getCurrentSession().load(UserAuto.class, id);
        if (null != userAuto) {
            sessionFactory.getCurrentSession().delete(userAuto);
        }
    }

    @Override
    public UserAuto getUserAuto(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from UserAuto where id = :id");
        query.setLong("id", id);
        return (UserAuto) query.uniqueResult();
    }
    
    @Override
    public UserAuto checkChain(UserAuto chain) {
        Query query = sessionFactory.getCurrentSession().createQuery("from UserAuto where user.telephone = :telephone and auto.number = :number and Status = 'Active'");
        query.setString("telephone", chain.getUser().getTelephone());
        query.setString("number", chain.getAuto().getNumber());
        return (UserAuto) query.uniqueResult();
    }      
}
