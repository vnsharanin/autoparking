package com.shar.dao;

import com.shar.model.Auto;
import java.util.List;

public interface AutoDAO {

    List<Auto> listAutos();

    void saveAuto(Auto auto);

    void deleteAuto(int id);

    Auto getAuto(int id);
}
