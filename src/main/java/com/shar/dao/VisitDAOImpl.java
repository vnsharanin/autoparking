package com.shar.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.shar.model.Visit;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Repository("VisitDAO")
public class VisitDAOImpl implements VisitDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<Visit> listVisits() {
        try {
            return sessionFactory.getCurrentSession().createQuery("from Visit").list();
        } catch (Exception ex) {
            throw new RuntimeException("Can't get from DB", ex);
        }
    }

    public void saveVisit(Visit visit) {
        sessionFactory.getCurrentSession().saveOrUpdate(visit);
    }

    @Override
    public void deleteVisit(int id) {
        Visit visit = (Visit) sessionFactory.getCurrentSession().load(Visit.class, id);
        if (null != visit) {
            sessionFactory.getCurrentSession().delete(visit);
        }
    }

    @Override
    public Visit getVisit(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Visit where id = :id");
        query.setLong("id", id);
        return (Visit) query.uniqueResult();
    }
}
