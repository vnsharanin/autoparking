package com.shar.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.shar.model.TariffAbonement;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Repository("TariffAbonementDAO")
public class TariffAbonementDAOImpl implements TariffAbonementDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<TariffAbonement> getActiveTariffAbonement() {
        try {
            return sessionFactory.getCurrentSession().createQuery("from TariffAbonement where Status = 'Active'").list();
        } catch (Exception ex) {
            throw new RuntimeException("Can't get from DB", ex);
        }
    }

    public void saveTariffAbonement(TariffAbonement tariffAbonement) {
        sessionFactory.getCurrentSession().saveOrUpdate(tariffAbonement);
    }

    @Override
    public List<TariffAbonement> getDisableTariffAbonement() {
        try {
            return sessionFactory.getCurrentSession().createQuery("from TariffAbonement where Status <> 'Active'").list();
        } catch (Exception ex) {
            throw new RuntimeException("Can't get from DB", ex);
        }
    }

    @Override
    public TariffAbonement getTariffAbonement(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from TariffAbonement where id = :id");
        query.setLong("id", id);
        return (TariffAbonement) query.uniqueResult();
    }
}
