package com.shar.dao;

import com.shar.model.VisitParameters;
import java.util.List;

public interface VisitParametersDAO {

    List<VisitParameters> getActiveVisitParameters();

    void saveVisitParameters(VisitParameters visitParameters);

    List<VisitParameters> getDisableVisitParameters();

    VisitParameters getVisitParameters(int id);
}
