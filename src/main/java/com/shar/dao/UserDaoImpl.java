package com.shar.dao;

import com.shar.model.BalanceOperation;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.shar.model.User;
import com.shar.model.UserRole;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;
    Transaction tx = null;

    public User findById(int id) {
        return getByKey(id); //to AbstractDao
    }

    public User findByEmail(String email) {
        Criteria crit = createEntityCriteria(); //to AbstractDao
        crit.add(Restrictions.eq("email", email));
        return (User) crit.uniqueResult();
    }

    public User findByTelephone(String telephone) {
        System.out.println(telephone);
        Criteria crit = createEntityCriteria(); //to AbstractDao
        crit.add(Restrictions.eq("telephone", telephone));
        return (User) crit.uniqueResult();
    }

    @Autowired
    RoleDAO roleDAO;
    
    public User saveUser(User user) {
        if (findByTelephone(user.getTelephone()) == null) {
            user.setStatus("Active");
            ShaPasswordEncoder sha = new ShaPasswordEncoder();
            user.setPassword(sha.encodePassword(user.getPassword(), ""));

            user.getBalanceOperationses().add(new BalanceOperation(user,"Initiation",0,0,"Initiation","Active"));
            user.getUserRoles().add(new UserRole(user,roleDAO.findByName("USER")));
            
            sessionFactory.getCurrentSession().save(user);
        } else {
            user.setStatus("Locked");
        }
        return user;
    }

    @SuppressWarnings("unchecked")
    public List<User> listUsers() {
        return sessionFactory.getCurrentSession().createQuery("from User").list();
    }
    
}
