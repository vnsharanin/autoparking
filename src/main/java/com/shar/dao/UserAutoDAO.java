package com.shar.dao;

import com.shar.model.UserAuto;
import java.util.List;

public interface UserAutoDAO {

    List<UserAuto> listUserAutos();

    void saveUserAuto(UserAuto userAuto);

    void deleteUserAuto(int id);

    UserAuto getUserAuto(int id);
    
    UserAuto checkChain(UserAuto chain);
}
