package com.shar.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.shar.model.Auto;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Repository("AutoDAO")
public class AutoDAOImpl implements AutoDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<Auto> listAutos() {
        try {
            return sessionFactory.getCurrentSession().createQuery("from Auto").list();
        } catch (Exception ex) {
            throw new RuntimeException("Can't get from DB", ex);
        }
    }

    public void saveAuto(Auto auto) {
        sessionFactory.getCurrentSession().saveOrUpdate(auto);
    }

    @Override
    public void deleteAuto(int id) {
        Auto auto = (Auto) sessionFactory.getCurrentSession().load(Auto.class, id);
        if (null != auto) {
            sessionFactory.getCurrentSession().delete(auto);
        }
    }

    @Override
    public Auto getAuto(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Auto where id = :id");
        query.setLong("id", id);
        return (Auto) query.uniqueResult();
    }
}
