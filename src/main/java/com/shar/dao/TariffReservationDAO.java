package com.shar.dao;

import com.shar.model.TariffReservation;
import java.util.List;

public interface TariffReservationDAO {

    void saveTariffReservation(TariffReservation tariffReservation);

    TariffReservation getTariffReservation(int id);

    TariffReservation getActiveTariffReservation();

    List<TariffReservation> getDisableTariffReservation();
}
