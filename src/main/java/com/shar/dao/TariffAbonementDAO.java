package com.shar.dao;

import com.shar.model.TariffAbonement;
import java.util.List;

public interface TariffAbonementDAO {

    List<TariffAbonement> getActiveTariffAbonement();

    void saveTariffAbonement(TariffAbonement tariffAbonement);

    List<TariffAbonement> getDisableTariffAbonement();

    TariffAbonement getTariffAbonement(int id);
}
