package com.shar.dao;

import com.shar.model.Role;
import java.util.List;

public interface RoleDAO {
        List<Role> listRoles();
	Role findById(int id);
        Role findByName(String role);
	void saveRole(Role role);   
}
