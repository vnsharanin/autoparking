package com.shar.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.shar.model.TariffPlace;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Repository("TariffPlaceDAO")
public class TariffPlaceDAOImpl implements TariffPlaceDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<TariffPlace> getActiveTariffPlace() {
        try {
            return sessionFactory.getCurrentSession().createQuery("from TariffPlace where Status='Active'").list();
        } catch (Exception ex) {
            throw new RuntimeException("Can't get from DB", ex);
        }
    }

    public void saveTariffPlace(TariffPlace tariffPlace) {
        sessionFactory.getCurrentSession().saveOrUpdate(tariffPlace);
    }

    @Override
    public List<TariffPlace> getDisableTariffPlace() {
        try {
            return sessionFactory.getCurrentSession().createQuery("from TariffPlace where Status<>'Active'").list();
        } catch (Exception ex) {
            throw new RuntimeException("Can't get from DB", ex);
        }
    }

    @Override
    public TariffPlace getTariffPlace(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from TariffPlace where id = :id");
        query.setLong("id", id);
        return (TariffPlace) query.uniqueResult();
    }
}
