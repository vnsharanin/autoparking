package com.shar.dao;

import com.shar.model.UserAbonement;
import java.util.List;

public interface UserAbonementDAO {

    List<UserAbonement> listUserAbonements();

    void saveUserAbonement(UserAbonement userAbonement);

    void deleteUserAbonement(int id);

    UserAbonement getUserAbonement(int id);
}
