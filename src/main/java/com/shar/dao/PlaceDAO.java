package com.shar.dao;

import com.shar.model.Place;
import java.util.List;

public interface PlaceDAO {

    List<Place> listPlaces();

    void savePlace(Place place);

    void deletePlace(int id);

    Place getPlace(int id);
    Place findPlace();
}
