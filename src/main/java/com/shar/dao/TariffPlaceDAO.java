package com.shar.dao;

import com.shar.model.TariffPlace;
import java.util.List;

public interface TariffPlaceDAO {

    List<TariffPlace> getActiveTariffPlace();

    void saveTariffPlace(TariffPlace tariffPlace);

    List<TariffPlace> getDisableTariffPlace();

    TariffPlace getTariffPlace(int id);
}
