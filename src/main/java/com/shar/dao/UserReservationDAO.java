package com.shar.dao;

import com.shar.model.UserReservation;
import java.util.List;

public interface UserReservationDAO {

    List<UserReservation> listUserReservations();

    void saveUserReservation(UserReservation userReservation);

    void deleteUserReservation(int id);

    UserReservation getUserReservation(int id);
}
