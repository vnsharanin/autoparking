package com.shar.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.shar.model.UserAbonement;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Repository("UserAbonementDAO")
public class UserAbonementDAOImpl implements UserAbonementDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<UserAbonement> listUserAbonements() {
        try {
            return sessionFactory.getCurrentSession().createQuery("from UserAbonement").list();
        } catch (Exception ex) {
            throw new RuntimeException("Can't get from DB", ex);
        }
    }

    public void saveUserAbonement(UserAbonement userAbonement) {
        sessionFactory.getCurrentSession().saveOrUpdate(userAbonement);
    }

    @Override
    public void deleteUserAbonement(int id) {
        UserAbonement userAbonement = (UserAbonement) sessionFactory.getCurrentSession().load(UserAbonement.class, id);
        if (null != userAbonement) {
            sessionFactory.getCurrentSession().delete(userAbonement);
        }
    }

    @Override
    public UserAbonement getUserAbonement(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from UserAbonement where id = :id");
        query.setLong("id", id);
        return (UserAbonement) query.uniqueResult();
    }
}
