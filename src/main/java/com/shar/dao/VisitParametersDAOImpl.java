package com.shar.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.shar.model.VisitParameters;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Repository("VisitParametersDAO")
public class VisitParametersDAOImpl implements VisitParametersDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<VisitParameters> getActiveVisitParameters() {
        try {
            return sessionFactory.getCurrentSession().createQuery("from VisitParameters where status = 'Active'").list();
        } catch (Exception ex) {
            throw new RuntimeException("Can't get from DB", ex);
        }
    }

    public void saveVisitParameters(VisitParameters visitParameters) {
        sessionFactory.getCurrentSession().saveOrUpdate(visitParameters);
    }

    @Override
    public List<VisitParameters> getDisableVisitParameters() {
       /* VisitParameters visitParameters = (VisitParameters) sessionFactory.getCurrentSession().load(VisitParameters.class, id);
        if (null != visitParameters) {
            sessionFactory.getCurrentSession().delete(visitParameters);
        }*/
         try {
            return sessionFactory.getCurrentSession().createQuery("from VisitParameters where status <> 'Active'").list();
        } catch (Exception ex) {
            throw new RuntimeException("Can't get from DB", ex);
        }       
    }

    @Override
    public VisitParameters getVisitParameters(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from VisitParameters where id = :id");
        query.setLong("id", id);
        return (VisitParameters) query.uniqueResult();
    }
}
