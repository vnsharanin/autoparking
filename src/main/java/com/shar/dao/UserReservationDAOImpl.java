package com.shar.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.shar.model.UserReservation;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Repository("UserReservationDAO")
public class UserReservationDAOImpl implements UserReservationDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<UserReservation> listUserReservations() {
        try {
            return sessionFactory.getCurrentSession().createQuery("from UserReservation").list();
        } catch (Exception ex) {
            throw new RuntimeException("Can't get from DB", ex);
        }
    }

    public void saveUserReservation(UserReservation userReservation) {
        sessionFactory.getCurrentSession().saveOrUpdate(userReservation);
    }

    @Override
    public void deleteUserReservation(int id) {
        UserReservation userReservation = (UserReservation) sessionFactory.getCurrentSession().load(UserReservation.class, id);
        if (null != userReservation) {
            sessionFactory.getCurrentSession().delete(userReservation);
        }
    }

    @Override
    public UserReservation getUserReservation(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from UserReservation where id = :id");
        query.setLong("id", id);
        return (UserReservation) query.uniqueResult();
    }
}
