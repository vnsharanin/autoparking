package com.shar.dao;

import com.shar.model.BalanceOperation;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("BalanceOperationDAO")
public class BalanceOperationDAOImpl implements BalanceOperationDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<BalanceOperation> listBalanceOperations() {
        try {
            return sessionFactory.getCurrentSession().createQuery("from BalanceOperation").list();
        } catch (Exception ex) {
            throw new RuntimeException("Can't get from DB", ex);
        }
    }

    public void saveBalanceOperation(BalanceOperation balanceOperation) {
        sessionFactory.getCurrentSession().saveOrUpdate(balanceOperation);
    }

    @Override
    public void deleteBalanceOperation(int id) {
        BalanceOperation balanceOperation = (BalanceOperation) sessionFactory.getCurrentSession().load(BalanceOperation.class, id);
        if (null != balanceOperation) {
            sessionFactory.getCurrentSession().delete(balanceOperation);
        }
    }

    @Override
    public BalanceOperation getBalanceOperation(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from BalanceOperation where id = :id");
        query.setLong("id", id);
        return (BalanceOperation) query.uniqueResult();
    }
}
