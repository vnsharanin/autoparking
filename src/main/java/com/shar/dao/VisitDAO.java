package com.shar.dao;

import com.shar.model.Visit;
import java.util.List;

public interface VisitDAO {

    List<Visit> listVisits();

    void saveVisit(Visit visit);

    void deleteVisit(int id);

    Visit getVisit(int id);
}
