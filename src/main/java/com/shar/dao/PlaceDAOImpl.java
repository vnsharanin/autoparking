package com.shar.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.shar.model.Place;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Repository("PlaceDAO")
public class PlaceDAOImpl implements PlaceDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<Place> listPlaces() {
        try {
            return sessionFactory.getCurrentSession().createQuery("from Place").list();
        } catch (Exception ex) {
            throw new RuntimeException("Can't get from DB", ex);
        }
    }

    public void savePlace(Place place) {
        sessionFactory.getCurrentSession().saveOrUpdate(place);
    }

    @Override
    public void deletePlace(int id) {
        Place place = (Place) sessionFactory.getCurrentSession().load(Place.class, id);
        if (null != place) {
            sessionFactory.getCurrentSession().delete(place);
        }
    }

    @Override
    public Place getPlace(int id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Place where id = :id");
        query.setLong("id", id);
        return (Place) query.uniqueResult();
    }
    
    @Override
    public Place findPlace(){
        Query query = sessionFactory.getCurrentSession().createQuery("from Place where Status = :status limit 1");
        query.setString("status", "Free");
        return (Place) query.uniqueResult();        
    }
}
