package com.shar.service;

import com.shar.model.TariffPlace;
import java.util.List;

public interface TariffPlaceService {

    List<TariffPlace> getActiveTariffPlace();

    void saveTariffPlace(TariffPlace tariffPlace);

    List<TariffPlace> getDisableTariffPlace();

    TariffPlace getTariffPlace(int id);
}
