package com.shar.service;

import com.shar.dao.UserAbonementDAO;
import java.util.List;

import org.springframework.stereotype.Service;

import com.shar.model.UserAbonement;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service("UserAbonementService")
@Transactional
public class UserAbonementServiceImpl implements UserAbonementService {

    @Autowired
    private UserAbonementDAO userAbonementDAO;

    @SuppressWarnings("unchecked")
    @Override
    public List<UserAbonement> listUserAbonements() {
        return userAbonementDAO.listUserAbonements();
    }

    public void saveUserAbonement(UserAbonement userAbonement) {
        userAbonementDAO.saveUserAbonement(userAbonement);
    }

    @Override
    public void deleteUserAbonement(int id) {
        userAbonementDAO.deleteUserAbonement(id);
    }

    @Override
    public UserAbonement getUserAbonement(int id) {
        return userAbonementDAO.getUserAbonement(id);
    }
}
