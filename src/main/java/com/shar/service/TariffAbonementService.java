package com.shar.service;

import com.shar.model.TariffAbonement;
import java.util.List;

public interface TariffAbonementService {

    List<TariffAbonement> getActiveTariffAbonement();

    void saveTariffAbonement(TariffAbonement tariffAbonement);

    List<TariffAbonement> getDisableTariffAbonement();

    TariffAbonement getTariffAbonement(int id);
}
