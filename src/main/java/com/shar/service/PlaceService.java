package com.shar.service;

import com.shar.model.Place;
import com.shar.model.UserReservation;
import java.util.List;
import java.util.Set;

public interface PlaceService {

    List<Place> listPlaces();

    void savePlace(Place place);

    void deletePlace(int id);

    Place getPlace(int id);
    
    Place findPlace();
    Place findPlaceOnReservation(Set<UserReservation> userReservations);
}
