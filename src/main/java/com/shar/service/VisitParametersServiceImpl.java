package com.shar.service;

import com.shar.dao.VisitParametersDAO;
import java.util.List;

import org.springframework.stereotype.Service;

import com.shar.model.VisitParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service("VisitParametersService")
@Transactional
public class VisitParametersServiceImpl implements VisitParametersService {

    @Autowired
    private VisitParametersDAO visitParametersDAO;

    @SuppressWarnings("unchecked")
    @Override
    public List<VisitParameters> getActiveVisitParameters() {
        return visitParametersDAO.getActiveVisitParameters();
    }

    public void saveVisitParameters(VisitParameters visitParameters) {
        visitParametersDAO.saveVisitParameters(visitParameters);
    }

    @Override
    public List<VisitParameters> getDisableVisitParameters() {
        return visitParametersDAO.getDisableVisitParameters();
    }

    @Override
    public VisitParameters getVisitParameters(int id) {
        return visitParametersDAO.getVisitParameters(id);
    }
}
