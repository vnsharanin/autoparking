package com.shar.service;

import com.shar.dao.TariffAbonementDAO;
import java.util.List;

import org.springframework.stereotype.Service;

import com.shar.model.TariffAbonement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service("TariffAbonementService")
@Transactional
public class TariffAbonementServiceImpl implements TariffAbonementService {

    @Autowired
    private TariffAbonementDAO tariffAbonementDAO;

    @SuppressWarnings("unchecked")
    @Override
    public List<TariffAbonement> getActiveTariffAbonement() {
        return tariffAbonementDAO.getActiveTariffAbonement();
    }

    public void saveTariffAbonement(TariffAbonement tariffAbonement) {
        tariffAbonementDAO.saveTariffAbonement(tariffAbonement);
    }

    @Override
    public List<TariffAbonement> getDisableTariffAbonement() {
        return tariffAbonementDAO.getDisableTariffAbonement();
    }

    @Override
    public TariffAbonement getTariffAbonement(int id) {
        return tariffAbonementDAO.getTariffAbonement(id);
    }
}
