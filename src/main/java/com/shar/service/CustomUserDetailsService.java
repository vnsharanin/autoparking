package com.shar.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shar.model.User;
import com.shar.model.UserRole;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String telephone)
            throws UsernameNotFoundException {
        User user = userService.findByTelephone(telephone.replace(" ", ""));

        if (user == null) {
            throw new UsernameNotFoundException("Username not found");
        }
        return new org.springframework.security.core.userdetails.User(user.getTelephone(), user.getPassword(),
                user.getStatus().equals("Active"), true, true, true, getGrantedAuthorities(user));
    }

    private List<GrantedAuthority> getGrantedAuthorities(User user) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        try {
            for (UserRole userRole : user.getUserRoles()) {
                authorities.add(new SimpleGrantedAuthority("ROLE_" + userRole.getRole().getName()));
            }
        } catch (Exception ex) {
            throw new RuntimeException("Can't get roles", ex);
        }
        return authorities;
    }
}
