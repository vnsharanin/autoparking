package com.shar.service;

import com.shar.model.UserAbonement;
import java.util.List;

public interface UserAbonementService {

    List<UserAbonement> listUserAbonements();

    void saveUserAbonement(UserAbonement userAbonement);

    void deleteUserAbonement(int id);

    UserAbonement getUserAbonement(int id);
}
