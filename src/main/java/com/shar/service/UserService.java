package com.shar.service;

import com.shar.model.User;
import java.util.List;

public interface UserService {
        List<User> listUsers();
	User findById(int id);
	User saveUser(User user);
	User findByEmail(String email);
        User findByTelephone(String telephone);
}