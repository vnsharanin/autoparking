package com.shar.service;

import com.shar.model.BalanceOperation;
import java.util.List;

public interface BalanceOperationService {

    List<BalanceOperation> listBalanceOperations();

    void saveBalanceOperation(BalanceOperation balanceOperation);

    void deleteBalanceOperation(int id);

    BalanceOperation getBalanceOperation(int id);
}
