package com.shar.service;

import com.shar.dao.UserAutoDAO;
import java.util.List;

import org.springframework.stereotype.Service;

import com.shar.model.UserAuto;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service("UserAutoService")
@Transactional
public class UserAutoServiceImpl implements UserAutoService {

    @Autowired
    private UserAutoDAO userAutoDAO;

    @Autowired
    private UserService userService;    

    @Autowired
    private AutoService autoService;    
    
    @SuppressWarnings("unchecked")
    @Override
    public List<UserAuto> listUserAutos() {
        return userAutoDAO.listUserAutos();
    }

    public void saveUserAuto(UserAuto userAuto) {
        userAutoDAO.saveUserAuto(userAuto);
    }

    @Override
    public void deleteUserAuto(int id) {
        userAutoDAO.deleteUserAuto(id);
    }

    @Override
    public UserAuto getUserAuto(int id) {
        return userAutoDAO.getUserAuto(id);
    }
 
    @Override
    public UserAuto checkChain(UserAuto chain) {
           return userAutoDAO.checkChain(chain);
    }    
    
}
