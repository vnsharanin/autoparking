package com.shar.service;

import com.shar.model.Auto;
import java.util.List;

public interface AutoService {

    List<Auto> listAutos();

    void saveAuto(Auto auto);

    void deleteAuto(int id);

    Auto getAuto(int id);
}
