package com.shar.service;

import com.shar.model.Visit;
import java.util.List;

public interface VisitService {

    List<Visit> listVisits();

    void saveVisit(Visit visit);

    void deleteVisit(int id);

    Visit getVisit(int id);

    String resultRegisterIn(String login, String password, String number, String location);

    String resultRegisterOut(String login, String password, String number, String location);

}
