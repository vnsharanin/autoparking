package com.shar.service;

import com.shar.model.Role;
import com.shar.model.User;
import com.shar.model.UserRole;
import java.util.List;

public interface UserRoleService {
    List<Role> getRolesByUser(int id);
    List<User> getUsersByRole(int id);
    List<UserRole> getUserRoleByUserId(int userId);
    //Map<User, Role> getUsersRoles();
    void saveUserRole(int roleId, int userId);
    //void removeUserRole(int roleId, int userId);    
}
