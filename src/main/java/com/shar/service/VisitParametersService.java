package com.shar.service;

import com.shar.model.VisitParameters;
import java.util.List;

public interface VisitParametersService {

    List<VisitParameters> getActiveVisitParameters();

    void saveVisitParameters(VisitParameters visitParameters);

    List<VisitParameters> getDisableVisitParameters();

    VisitParameters getVisitParameters(int id);
}
