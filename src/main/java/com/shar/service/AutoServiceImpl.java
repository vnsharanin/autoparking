package com.shar.service;

import com.shar.dao.AutoDAO;
import java.util.List;

import com.shar.model.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("AutoService")
@Transactional
public class AutoServiceImpl implements AutoService {

    @Autowired
    private AutoDAO autoDAO;

    @SuppressWarnings("unchecked")
    @Override
    public List<Auto> listAutos() {
        return autoDAO.listAutos();
    }

    public void saveAuto(Auto auto) {
        autoDAO.saveAuto(auto);
    }

    @Override
    public void deleteAuto(int id) {
        autoDAO.deleteAuto(id);
    }

    @Override
    public Auto getAuto(int id) {
        return autoDAO.getAuto(id);
    }
}
