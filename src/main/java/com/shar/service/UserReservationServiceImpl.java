package com.shar.service;

import com.shar.dao.UserReservationDAO;
import java.util.List;

import org.springframework.stereotype.Service;

import com.shar.model.UserReservation;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service("UserReservationService")
@Transactional
public class UserReservationServiceImpl implements UserReservationService {

    @Autowired
    private UserReservationDAO userReservationDAO;

    @SuppressWarnings("unchecked")
    @Override
    public List<UserReservation> listUserReservations() {
        return userReservationDAO.listUserReservations();
    }

    public void saveUserReservation(UserReservation userReservation) {
        userReservationDAO.saveUserReservation(userReservation);
    }

    @Override
    public void deleteUserReservation(int id) {
        userReservationDAO.deleteUserReservation(id);
    }

    @Override
    public UserReservation getUserReservation(int id) {
        return userReservationDAO.getUserReservation(id);
    }
}
