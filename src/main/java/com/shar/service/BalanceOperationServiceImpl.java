package com.shar.service;

import com.shar.dao.BalanceOperationDAO;
import com.shar.model.BalanceOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("BalanceOperationService")
@Transactional
public class BalanceOperationServiceImpl implements BalanceOperationService {

    @Autowired
    private BalanceOperationDAO balanceOperationDAO;

    @SuppressWarnings("unchecked")
    @Override
    public List<BalanceOperation> listBalanceOperations() {
        return balanceOperationDAO.listBalanceOperations();
    }

    public void saveBalanceOperation(BalanceOperation balanceOperation) {
        balanceOperationDAO.saveBalanceOperation(balanceOperation);
    }

    @Override
    public void deleteBalanceOperation(int id) {
        balanceOperationDAO.deleteBalanceOperation(id);
    }

    @Override
    public BalanceOperation getBalanceOperation(int id) {
        return balanceOperationDAO.getBalanceOperation(id);
    }
}