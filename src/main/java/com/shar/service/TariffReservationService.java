package com.shar.service;

import com.shar.model.TariffReservation;
import java.util.List;

public interface TariffReservationService {

    void saveTariffReservation(TariffReservation tariffReservation);

    TariffReservation getTariffReservation(int id);

    TariffReservation getActiveTariffReservation();

    List<TariffReservation> getDisableTariffReservation();
}
