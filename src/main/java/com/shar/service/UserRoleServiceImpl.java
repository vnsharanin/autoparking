package com.shar.service;

import com.shar.dao.UserRoleDAO;
import com.shar.model.Role;
import com.shar.model.User;
import com.shar.model.UserRole;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("IUserRoleService")
@Transactional
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleDAO userRoleDAO;

    public List<UserRole> getUserRoleByUserId(int userId) {
        return userRoleDAO.getUserRoleByUserId(userId);
    }

    public List<User> getUsersByRole(int id) {
        return userRoleDAO.getUsersByRole(id);
    }

    public List<Role> getRolesByUser(int id) {
        return userRoleDAO.getRolesByUser(id);
    }

    public void saveUserRole(int roleId, int userId) {
        userRoleDAO.saveUserRole(roleId, userId);
    }
}
