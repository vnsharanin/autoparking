package com.shar.service;

import com.shar.model.UserAuto;
import java.util.List;

public interface UserAutoService {

    List<UserAuto> listUserAutos();

    void saveUserAuto(UserAuto userAuto);

    void deleteUserAuto(int id);

    UserAuto getUserAuto(int id);
    
    UserAuto checkChain(UserAuto chain);
}
