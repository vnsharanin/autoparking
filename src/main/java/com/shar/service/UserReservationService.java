package com.shar.service;

import com.shar.model.UserReservation;
import java.util.List;

public interface UserReservationService {

    List<UserReservation> listUserReservations();

    void saveUserReservation(UserReservation userReservation);

    void deleteUserReservation(int id);

    UserReservation getUserReservation(int id);
}
