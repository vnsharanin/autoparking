package com.shar.service;

import com.shar.dao.TariffReservationDAO;
import java.util.List;

import org.springframework.stereotype.Service;

import com.shar.model.TariffReservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service("TariffReservationService")
@Transactional
public class TariffReservationServiceImpl implements TariffReservationService {

    @Autowired
    private TariffReservationDAO tariffReservationDAO;

    public void saveTariffReservation(TariffReservation tariffReservation) {
        tariffReservationDAO.saveTariffReservation(tariffReservation);
    }

    @Override
    public TariffReservation getTariffReservation(int id) {
        return tariffReservationDAO.getTariffReservation(id);
    }
    @Override
    public TariffReservation getActiveTariffReservation(){
        return tariffReservationDAO.getActiveTariffReservation();
    }
    
    @Override
    public List<TariffReservation> getDisableTariffReservation(){
        return tariffReservationDAO.getDisableTariffReservation();
    }   
}
