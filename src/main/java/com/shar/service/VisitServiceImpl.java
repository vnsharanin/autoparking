package com.shar.service;

import com.shar.dao.VisitDAO;
import java.util.List;

import org.springframework.stereotype.Service;

import com.shar.model.Visit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service("VisitService")
@Transactional
public class VisitServiceImpl implements VisitService {

    @Autowired
    private VisitDAO visitDAO;

    @Autowired
    private UserAutoService userAutoService;

    @Autowired
    private PlaceService placeService;

    @SuppressWarnings("unchecked")
    @Override
    public List<Visit> listVisits() {
        return visitDAO.listVisits();
    }

    public void saveVisit(Visit visit) {
        visitDAO.saveVisit(visit);
    }

    @Override
    public void deleteVisit(int id) {
        visitDAO.deleteVisit(id);
    }

    @Override
    public Visit getVisit(int id) {
        return visitDAO.getVisit(id);
    }

    @Override
    public String resultRegisterIn(String login, String password, String number, String location) {
        Visit visit = new Visit(login, password, number, location);
        visit.setUserAuto(userAutoService.checkChain(visit.getUserAuto()));
        if (!visit.getUserAuto().getUser().getPassword().equals(password)) {
            return "0;Chain is'not found";
        }
        visit.setPlace(placeService.findPlaceOnReservation(visit.getUserAuto().getUser().getUserReservations()));
        if (visit.getPlace() == null) {
            visit.setPlace(placeService.findPlace());
        }
        visit.setDateIn(com.shar.configuration.MyDate.getCurrentDate());
        visit.setStatus("Open");
        visitDAO.saveVisit(visit);
        return "1;Success";
    }
    @Override
    public String resultRegisterOut(String login, String password, String number, String location) {
        Visit visit = new Visit(login, password, number, location);
        visit.setUserAuto(userAutoService.checkChain(visit.getUserAuto()));
        visit = null;//visitDAO.findVisit(visit.getUserAuto().getId());
        if (visit == null) {
            return "0;Chain is'not found";
        }
        visit.setDateOut(com.shar.configuration.MyDate.getCurrentDate());
        visit.setStatus("Close");
        visitDAO.saveVisit(visit);
        return "1;Success";
    }
}
