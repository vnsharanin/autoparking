package com.shar.service;

import com.shar.dao.TariffPlaceDAO;
import java.util.List;

import org.springframework.stereotype.Service;

import com.shar.model.TariffPlace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service("TariffPlaceService")
@Transactional
public class TariffPlaceServiceImpl implements TariffPlaceService {

    @Autowired
    private TariffPlaceDAO tariffPlaceDAO;

    @SuppressWarnings("unchecked")
    @Override
    public List<TariffPlace> getActiveTariffPlace() {
        return tariffPlaceDAO.getActiveTariffPlace();
    }

    public void saveTariffPlace(TariffPlace tariffPlace) {
        tariffPlaceDAO.saveTariffPlace(tariffPlace);
    }

    @Override
    public List<TariffPlace> getDisableTariffPlace() {
        return tariffPlaceDAO.getDisableTariffPlace();
    }

    @Override
    public TariffPlace getTariffPlace(int id) {
        return tariffPlaceDAO.getTariffPlace(id);
    }
}
