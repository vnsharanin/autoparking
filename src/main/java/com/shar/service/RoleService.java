package com.shar.service;

import com.shar.model.Role;
import java.util.List;

public interface RoleService {
        List<Role> listRoles();
	Role findById(int id);
        Role findByName(String role);
	void saveRole(Role role);      
}
