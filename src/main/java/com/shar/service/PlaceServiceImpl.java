package com.shar.service;

import com.shar.dao.PlaceDAO;
import java.util.List;

import org.springframework.stereotype.Service;

import com.shar.model.Place;
import com.shar.model.UserReservation;
import java.util.Set;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service("PlaceService")
@Transactional
public class PlaceServiceImpl implements PlaceService {

    @Autowired
    private PlaceDAO placeDAO;

    @SuppressWarnings("unchecked")
    @Override
    public List<Place> listPlaces() {
        return placeDAO.listPlaces();
    }

    public void savePlace(Place place) {
        placeDAO.savePlace(place);
    }

    @Override
    public void deletePlace(int id) {
        placeDAO.deletePlace(id);
    }

    @Override
    public Place getPlace(int id) {
        return placeDAO.getPlace(id);
    }

    @Override
    public Place findPlace() {
        return placeDAO.findPlace();
    }

    @Override
    public Place findPlaceOnReservation(Set<UserReservation> userReservations) {
        for (UserReservation reservation : userReservations) {
            if (reservation.getStatus().equals("Active"))
                    return reservation.getPlace();
        }
        return null;
    }
}
