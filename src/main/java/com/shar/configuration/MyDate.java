package com.shar.configuration;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyDate {
    public static String getCurrentDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy HH:mm");
        java.util.Date currentDate = new java.util.Date();
        return sdf.format(currentDate);
        /*String date = sdf.format(currentDate); 
        return (Date) sdf.parse(date);*/
    }
}
