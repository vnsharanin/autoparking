package com.shar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "users_reservations", catalog = "parking")
public class UserReservation implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "place")
    private Place place;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tariff_reservation", nullable = false)
    private TariffReservation tariffReservation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user", nullable = false)
    private User user;

    @Column(name = "date_connection")
    private String dateConnection;

    @Column(name = "date_out_from_activity")
    private String dateOutFromActivity;

    @Column(name = "alternative_location_place")
    private Long alternativeLocationPlaceId;

    @Column(name = "status", nullable = false)
    private String status;

    public UserReservation() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public TariffReservation getTariffsReservation() {
        return tariffReservation;
    }

    public void setTariffsReservation(TariffReservation tariffsReservation) {
        this.tariffReservation = tariffsReservation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDateConnection() {
        return dateConnection;
    }

    public void setDateConnection(String dateConnection) {
        this.dateConnection = dateConnection;
    }

    public String getDateOutFromActivity() {
        return dateOutFromActivity;
    }

    public void setDateOutFromActivity(String dateOutFromActivity) {
        this.dateOutFromActivity = dateOutFromActivity;
    }

    public Long getAlternativeLocationPlaceId() {
        return alternativeLocationPlaceId;
    }

    public void setAlternativeLocationPlaceId(Long alternativeLocationPlaceId) {
        this.alternativeLocationPlaceId = alternativeLocationPlaceId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
