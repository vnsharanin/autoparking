package com.shar.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tariffs_reservation", catalog = "parking")
public class TariffReservation implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "first_free_time_in_minutes", nullable = false)
    private int firstFreeTimeInMinutes;

    @Column(name = "price_in_hour_after_free_time", nullable = false)
    private int priceInHourAfterFreeTime;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "time_of_application_in_hours", nullable = false)
    private int timeOfApplicationInHours;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "tariffReservation")
    private Set<UserReservation> userReservations = new HashSet<UserReservation>();

    public TariffReservation() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getFirstFreeTimeInMinutes() {
        return firstFreeTimeInMinutes;
    }

    public void setFirstFreeTimeInMinutes(int firstFreeTimeInMinutes) {
        this.firstFreeTimeInMinutes = firstFreeTimeInMinutes;
    }

    public int getPriceInHourAfterFreeTime() {
        return priceInHourAfterFreeTime;
    }

    public void setPriceInHourAfterFreeTime(int priceInHourAfterFreeTime) {
        this.priceInHourAfterFreeTime = priceInHourAfterFreeTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTimeOfApplicationInHours() {
        return timeOfApplicationInHours;
    }

    public void setTimeOfApplicationInHours(int timeOfApplicationInHours) {
        this.timeOfApplicationInHours = timeOfApplicationInHours;
    }

    public Set<UserReservation> getUsersReservations() {
        return userReservations;
    }

    public void setUsersReservations(Set<UserReservation> usersReservations) {
        this.userReservations = usersReservations;
    }

}
