package com.shar.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "Users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    //@NotEmpty
    @Email
    @Column(name = "EMAIL", nullable = true)
    private String email;

    @Column(name = "telephone", unique = true, nullable = false)
    private String telephone;

    @NotNull
    //@Size(min = 4)
    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "STATUS", nullable = false)
    private String status;// = Status.ACTIVE.getStatus();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private Set<UserAbonement> userAbonements = new HashSet<UserAbonement>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private Set<UserAuto> userAutos = new HashSet<UserAuto>();
 
    @Cascade({CascadeType.ALL})
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private Set<UserRole> userRoles = new HashSet<UserRole>();
    
    @Cascade({CascadeType.ALL})
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
     // @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    private Set<BalanceOperation> balanceOperations = new HashSet<BalanceOperation>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private Set<UserReservation> userReservations = new HashSet<UserReservation>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "configuration", nullable = false)
    private Configuration configuration;	
	
    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
	
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public Configuration getConfiguration() {
        return configuration;
    }	
	
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    /*
     public Set<Role> getRole() {
     return role;
     }

     public void setRole(Set<Role> role) {
     this.role = role;
     }*/

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                //.append(email)
                .append(telephone)
                //.append(password)
                .append(status)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof User)) {
            return false;
        }
        User other = (User) obj;
        if (id != other.id) {
            return false;
        }
        if (telephone == null) {
            if (other.telephone != null) {
                return false;
            }
        } else if (!telephone.equals(other.telephone)) {
            return false;
        }

        if (password == null) {
            if (other.password != null) {
                return false;
            }
        } else if (!password.equals(other.password)) {
            return false;
        }        
        
        return true;
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", ssoId=" + email + ", password=" + password
                + ", email=" + email + ", state=" + status + "]";
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Set<UserAbonement> getUserAbonements() {
        return userAbonements;
    }

    public void setUserAbonements(Set<UserAbonement> userAbonements) {
        this.userAbonements = userAbonements;
    }

    public Set<UserAuto> getUserAutos() {
        return userAutos;
    }

    public void setUserAutos(Set<UserAuto> userAutos) {
        this.userAutos = userAutos;
    }
    
    public Set<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }    

    public Set<BalanceOperation> getBalanceOperationses() {
        return balanceOperations;
    }

    public void setBalanceOperationses(Set<BalanceOperation> balanceOperationses) {
        this.balanceOperations = balanceOperationses;
    }

    public Set<UserReservation> getUserReservations() {
        return userReservations;
    }

    public void setUserReservations(Set<UserReservation> userReservations) {
        this.userReservations = userReservations;
    }

    public User() {
    }

    public User(String telephone, String password) {
        this.telephone = telephone;
        this.password = password;
    }
}
