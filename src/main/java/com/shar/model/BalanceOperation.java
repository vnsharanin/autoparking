package com.shar.model;

import com.shar.configuration.MyDate;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "balance_operations", catalog = "parking")
public class BalanceOperation implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @ManyToOne//(fetch = FetchType.LAZY)
    @JoinColumn(name = "user")//, nullable = false)
    private User user;

    @Column(name = "type_operation", nullable = false)
    private String typeOperation;

    @Column(name = "sum", nullable = false)
    private int sum;

    @Column(name = "current_balance", nullable = false)
    private int currentBalance;

    @Column(name = "description")
    private String description;

    @Column(name = "date_operation", nullable = false)
    private String dateOperation;

    @Column(name = "status", nullable = false)
    private String status;

    public BalanceOperation() {
    }

    public BalanceOperation(User user, String typeOperation, int sum, int currentBalance, String description, String status) {
        this.user = user;
        this.typeOperation = typeOperation;
        this.sum = sum;
        this.currentBalance = currentBalance;
        this.description = description;
        this.dateOperation = MyDate.getCurrentDate();
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUsers() {
        return user;
    }

    public void setUsers(User users) {
        this.user = users;
    }

    public String getTypeOperation() {
        return typeOperation;
    }

    public void setTypeOperation(String typeOperation) {
        this.typeOperation = typeOperation;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(int currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateOperation() {
        return dateOperation;
    }

    public void setDateOperation(String dateOperation) {
        this.dateOperation = dateOperation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
