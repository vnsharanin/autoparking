package com.shar.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "places")
public class Place implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tariff_on_place", nullable = false)
    private TariffPlace tariffPlace;

    @Column(name = "number", nullable = false)
    private int number;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "next_status")
    private String nextStatus;

    @Column(name = "replacing_place")
    private Long replacingPlaceId;

    /* @OneToMany(fetch=FetchType.LAZY, mappedBy="place")//���� ������������� �� ����� ���������
     private Set<UserAbonement> userAbonements = new HashSet<UserAbonement>();
      
     @OneToMany(fetch=FetchType.LAZY, mappedBy="place")
     private Set usersReservationses = new HashSet(0);*/
    public Place() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TariffPlace getTariffPlace() {
        return tariffPlace;
    }

    public void setTariffPlace(TariffPlace tariffPlace) {
        this.tariffPlace = tariffPlace;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNextStatus() {
        return nextStatus;
    }

    public void setNextStatus(String nextStatus) {
        this.nextStatus = nextStatus;
    }

    public Long getReplacingPlaceId() {
        return replacingPlaceId;
    }

    public void setReplacingPlaceId(Long replacingPlaceId) {
        this.replacingPlaceId = replacingPlaceId;
    }
}
