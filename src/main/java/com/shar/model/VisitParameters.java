package com.shar.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "visit_parameters", catalog = "parking")
public class VisitParameters implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "first_free_time_in_minutes", nullable = false)
    private int firstFreeTimeInMinutes;

    @Column(name = "first_free_time_on_cashing", nullable = false)
    private int firstFreeTimeOnCashing;

    @Column(name = "Status", nullable = false)
    private String status;

    public VisitParameters() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getFirstFreeTimeInMinutes() {
        return firstFreeTimeInMinutes;
    }

    public void setFirstFreeTimeInMinutes(int firstFreeTimeInMinutes) {
        this.firstFreeTimeInMinutes = firstFreeTimeInMinutes;
    }

    public int getFirstFreeTimeOnCashing() {
        return firstFreeTimeOnCashing;
    }

    public void setFirstFreeTimeOnCashing(int firstFreeTimeOnCashing) {
        this.firstFreeTimeOnCashing = firstFreeTimeOnCashing;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
