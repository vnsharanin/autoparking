package com.shar.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tariffs_places")
public class TariffPlace implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "climate_control", nullable = false, length = 3)
    private String climateControl;

    @Column(name = "type", nullable = false, length = 6)
    private String type;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "price_in_hour_without_abonement", nullable = false)
    private Integer priceInHourWithoutAbonement;

    @Column(name = "price_in_hour_with_abonement", nullable = false)
    private int priceInHourWithAbonement;

    public TariffPlace() {
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClimateControl() {
        return climateControl;
    }

    public void setClimateControl(String climateControl) {
        this.climateControl = climateControl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getPriceInHourWithoutAbonement() {
        return priceInHourWithoutAbonement;
    }

    public void setPriceInHourWithoutAbonement(Integer priceInHourWithoutAbonement) {
        this.priceInHourWithoutAbonement = priceInHourWithoutAbonement;
    }

    public int getPriceInHourWithAbonement() {
        return priceInHourWithAbonement;
    }

    public void setPriceInHourWithAbonement(int priceInHourWithAbonement) {
        this.priceInHourWithAbonement = priceInHourWithAbonement;
    }
}
