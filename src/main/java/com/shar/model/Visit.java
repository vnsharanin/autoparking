package com.shar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "visits", catalog = "parking")
public class Visit implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "place", nullable = false)
    private Place place;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_auto", nullable = false)
    private UserAuto userAuto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "visit_parameters", nullable = false)
    private VisitParameters visitParameters;

    @Column(name = "date_in", nullable = false)
    private String dateIn;

    @Column(name = "date_out")
    private String dateOut;

    @Column(name = "first_attempt_go_out")
    private String firstAttemptGoOut;

    @Column(name = "next_attempt_go_out")
    private String nextAttemptGoOut;

    @Column(name = "Status", nullable = false)
    private String status;

    public Visit() {
    }

    public Visit(String login,String password,String number,String location) {
        this.userAuto.setUser(new User(login, password));
        this.userAuto.setAuto(new Auto(number));
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public UserAuto getUserAuto() {
        return userAuto;
    }

    public void setUserAuto(UserAuto userAuto) {
        this.userAuto = userAuto;
    }

    public VisitParameters getVisitParameters() {
        return visitParameters;
    }

    public void setVisitParameters(VisitParameters visitParameters) {
        this.visitParameters = visitParameters;
    }

    public String getDateIn() {
        return dateIn;
    }

    public void setDateIn(String dateIn) {
        this.dateIn = dateIn;
    }

    public String getDateOut() {
        return dateOut;
    }

    public void setDateOut(String dateOut) {
        this.dateOut = dateOut;
    }

    public String getFirstAttemptGoOut() {
        return firstAttemptGoOut;
    }

    public void setFirstAttemptGoOut(String firstAttemptGoOut) {
        this.firstAttemptGoOut = firstAttemptGoOut;
    }

    public String getNextAttemptGoOut() {
        return nextAttemptGoOut;
    }

    public void setNextAttemptGoOut(String nextAttemptGoOut) {
        this.nextAttemptGoOut = nextAttemptGoOut;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
