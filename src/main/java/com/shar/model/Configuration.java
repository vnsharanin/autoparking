package com.shar.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "Configurations")
public class Configuration {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

	@Column(name = "isChainToPlace")
	private Long isChainToPlace;
	
	@Column(name = "searchPlaceAlgorithm")
	private Long searchPlaceAlgorithm;

	@Column(name = "isDefault")
	private Long isDefault;

    public Configuration() {
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
	
    public void setIsChainToPlace(Long isChainToPlace) {
        this.isChainToPlace = isChainToPlace;
    }

    public Long getIsChainToPlace() {
        return isChainToPlace;
    }

    public void setSearchPlaceAlgorithm(Long searchPlaceAlgorithm) {
        this.searchPlaceAlgorithm = searchPlaceAlgorithm;
    }

    public Long getSearchPlaceAlgorithm() {
        return searchPlaceAlgorithm;
    }

    public void setIsDefault(Long isDefault) {
        this.isDefault = isDefault;
    }

    public Long getIsDefault() {
        return isDefault;
    }	
}