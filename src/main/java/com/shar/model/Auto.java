package com.shar.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "autos", catalog = "parking")
public class Auto implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "number", nullable = false, length = 10)
    private String number;

    @Column(name = "scanner_as")
    private String scannerAs;

    /*   @JoinColumn(name="auto_id")
     @OneToMany(fetch=FetchType.EAGER, mappedBy="autos")
     private Set<UserAuto> usersAutoses = new HashSet<UserAuto>();*/
    public Auto() {
    }
    
    public Auto(String number) {
        this.number = number;
    }    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getScannerAs() {
        return scannerAs;
    }

    public void setScannerAs(String scannerAs) {
        this.scannerAs = scannerAs;
    }

}
