package com.shar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "users_abonements", catalog = "parking")
public class UserAbonement implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "place")
    private Place place;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "abonement", nullable = false)
    private TariffAbonement tariffsAbonements;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user", nullable = false)
    private User user;

    @Column(name = "connection_date", nullable = false)
    private String connectionDate;

    @Column(name = "status", nullable = false, length = 20)
    private String status;

    @Column(name = "amount_completed_visits", nullable = false)
    private int amountCompletedVisits;

    @Column(name = "date_out_from_activity")
    private String dateOutFromActivity;

    public UserAbonement() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public TariffAbonement getTariffsAbonements() {
        return tariffsAbonements;
    }

    public void setTariffsAbonements(TariffAbonement tariffsAbonements) {
        this.tariffsAbonements = tariffsAbonements;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getConnectionDate() {
        return connectionDate;
    }

    public void setConnectionDate(String connectionDate) {
        this.connectionDate = connectionDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getAmountCompletedVisits() {
        return amountCompletedVisits;
    }

    public void setAmountCompletedVisits(int amountCompletedVisits) {
        this.amountCompletedVisits = amountCompletedVisits;
    }

    public String getDateOutFromActivity() {
        return dateOutFromActivity;
    }

    public void setDateOutFromActivity(String dateOutFromActivity) {
        this.dateOutFromActivity = dateOutFromActivity;
    }

}
