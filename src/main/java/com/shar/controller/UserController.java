﻿package com.shar.controller;

import com.shar.model.User;
import com.shar.service.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.shar.api.SMSCSender;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = {"/register"}, method = RequestMethod.POST)
  //  @ResponseBody
    public String addUser(HttpServletRequest request) {
       
        String telephone = request.getParameter("telephone").replace(" ", "");
        String password = request.getParameter("gpassword");
         if (!telephone.matches("^\\+7\\([0-9]{3}\\)[0-9]{3}-[0-9]{4}$")) { System.out.println("Incorrect number format!"); }
         else
        if(userService.saveUser(new User(telephone,password)).getStatus().equals("Active")) {
         /*   SMSCSender sd= new SMSCSender("vsharanin", "123654789s", "utf-8", true);
            sd.sendSms(telephone, "Password:"+password, 1, "", "", 0, "", "");
            sd.getSmsCost(telephone, "Вы успешно зарегистрированы!", 0, 0, "", "");  
            sd.getBalance();*/
        } //else when I make ajax - return "error message as JSON"
        
       System.out.println("PHONE: "+telephone + " PASSWORD: "+password+" ");
        
       return "redirect:/home";
    }
    
    @RequestMapping("/admin/users")
    public String listUsers(ModelMap model) {
        model.addAttribute("listOfUsers", userService.listUsers());
        return "users";
    }
    
    
}
