package com.shar.controller.user;

import com.shar.service.TariffAbonementService;
import com.shar.service.TariffReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserServiceController {
   /* @RequestMapping("/user/reservations")
    public String reservation(ModelMap model) {
        model.addAttribute("typeLeftMenu", "services");
        return "users/Reservations";
    }  */
    
    
    @Autowired
    TariffReservationService tariffReservationService;   

    @Autowired
    TariffAbonementService tariffAbonementService;
    
    @RequestMapping("/user/services/main")
    public String visitorServiceMain(ModelMap model) {
        model.addAttribute("active", "services");
        model.addAttribute("vertical_menu", "main");
        return "user/services/main";
    }
    
    @RequestMapping("/user/services/reservation")
    public String visitorServiceReservation(ModelMap model) {
        model.addAttribute("active", "services");
        model.addAttribute("vertical_menu", "reservation");
        model.addAttribute("active_tariff_reservation", tariffReservationService.getActiveTariffReservation());
        return "user/services/reservation";
    }
    
    @RequestMapping("/user/services/abonement")
    public String visitorServiceAbonement(ModelMap model) {
        model.addAttribute("active", "services");
        model.addAttribute("vertical_menu", "abonement");
        model.addAttribute("tariffs_abonement", tariffAbonementService.getActiveTariffAbonement());
        return "user/services/abonement";
    }
    
    @RequestMapping("/user/services/balance")
    public String visitorServiceBalance(ModelMap model) {
        model.addAttribute("active", "services");
        model.addAttribute("vertical_menu", "balance");
        model.addAttribute("tariffs_abonement", tariffAbonementService.getActiveTariffAbonement());
        return "user/services/abonement";
    }
}
