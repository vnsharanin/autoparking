package com.shar.controller.user;

import com.shar.service.AutoService;
import com.shar.service.TariffPlaceService;
import com.shar.service.UserService;
import com.shar.service.VisitParametersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserMonitoringController {

    @Autowired
    TariffPlaceService tariffPlaceService;

    @Autowired
    VisitParametersService visitParametersService;    
    
    @RequestMapping("/user/monitoring/main")
    public String visitorMonitoringMain(ModelMap model) {
        model.addAttribute("active", "monitoring");
        model.addAttribute("visits_parameters", visitParametersService.getActiveVisitParameters());
        model.addAttribute("tariff_place", tariffPlaceService.getActiveTariffPlace());        
        return "user/monitoring/main";
    }
    @RequestMapping("/user/monitoring/contacts")
    public String visitorMonitoringContacts(ModelMap model) {
        model.addAttribute("active", "monitoring");
        return "user/monitoring/contacts";
    }       
    
    @Autowired
    UserService userService;
    
  /*  @Autowired
    IUserDetailsService userDetailsService;    */
    
    //User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    @RequestMapping("/Personal/Auto")
    public String listAutos(ModelMap model) {
      //  model.addAttribute("autos", userService.findById(1).getUserAutos());
        return "Personal/Auto";
    }
}