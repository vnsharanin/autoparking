package com.shar.controller.visitor;

import com.shar.model.TariffPlace;
import com.shar.service.TariffPlaceService;
import com.shar.service.VisitParametersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class VisitorMonitoringController {

    @Autowired
    TariffPlaceService tariffPlaceService;

    @Autowired
    VisitParametersService visitParametersService;    
    
    @RequestMapping("/visitor/monitoring/main")
    public String visitorMonitoringMain(ModelMap model) {
        model.addAttribute("active", "monitoring");
        model.addAttribute("visits_parameters", visitParametersService.getActiveVisitParameters());
        model.addAttribute("tariff_place", tariffPlaceService.getActiveTariffPlace());        
        return "visitor/monitoring/main";
    }
    @RequestMapping("/visitor/monitoring/contacts")
    public String visitorMonitoringContacts(ModelMap model) {
        model.addAttribute("active", "monitoring");
        return "visitor/monitoring/contacts";
    }    
    
}
