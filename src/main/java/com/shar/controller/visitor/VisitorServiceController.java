package com.shar.controller.visitor;

import com.shar.service.TariffAbonementService;
import com.shar.service.TariffReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class VisitorServiceController {

    @Autowired
    TariffReservationService tariffReservationService;   

    @Autowired
    TariffAbonementService tariffAbonementService;
    
    @RequestMapping("/visitor/services/main")
    public String visitorServiceMain(ModelMap model) {
        model.addAttribute("active", "services");
        return "visitor/services/main";
    }
    
    @RequestMapping("/visitor/services/reservation")
    public String visitorServiceReservation(ModelMap model) {
        model.addAttribute("active", "services");
        model.addAttribute("active_tariff_reservation", tariffReservationService.getActiveTariffReservation());
        return "visitor/services/reservation";
    }
    
    @RequestMapping("/visitor/services/abonement")
    public String visitorServiceAbonement(ModelMap model) {
        model.addAttribute("active", "services");
        model.addAttribute("tariffs_abonement", tariffAbonementService.getActiveTariffAbonement());
        return "visitor/services/abonement";
    }    
}
