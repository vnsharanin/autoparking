package com.shar.controller.admin;

import com.shar.model.TariffPlace;
import com.shar.service.TariffPlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AdminMonitoringController {

    @Autowired
    TariffPlaceService tariffPlaceService;

    @RequestMapping("/admin/monitoring/tariff_places")
    public String adminTariffPlaces(ModelMap model) {
        model.addAttribute("active", "monitoring");
        model.addAttribute("tariffplaces", tariffPlaceService.getActiveTariffPlace());
        return "/admin/monitoring/tariff_places";
    }
    
    @RequestMapping("/admin/monitoring/main")
    public String adminMonitoringMain(ModelMap model) {
        model.addAttribute("active", "monitoring");
        return "admin/monitoring/main";
    }  
}
