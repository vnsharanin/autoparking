package com.shar.controller.subsystem;

import com.shar.service.VisitService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class VisitController {
    @Autowired
    VisitService visitService;    
    
    @RequestMapping(value = {"/RegisterIn"}, method = RequestMethod.GET)
    @ResponseBody
    public String RegisterIn(HttpServletRequest request) {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String number = request.getParameter("number");
        String location = request.getParameter("location");
        return visitService.resultRegisterIn(login, password,number,location);
    }
    @RequestMapping(value = {"/RegisterOut"}, method = RequestMethod.GET)
    @ResponseBody
    public String RegisterOut(HttpServletRequest request) {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String number = request.getParameter("number");
        String location = request.getParameter("location");
        return visitService.resultRegisterOut(login, password,number,location);
    }
}
