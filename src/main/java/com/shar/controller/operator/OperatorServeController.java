package com.shar.controller.operator;

import com.shar.model.TariffPlace;
import com.shar.service.TariffPlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class OperatorServeController {
    @Autowired
    TariffPlaceService tariffPlaceService;
    
    @RequestMapping("/operator/serve/main")
    public String operatorServeMain(ModelMap model) {
        model.addAttribute("active", "serve");
        return "operator/serve/main";
    }      
    @RequestMapping("/operator/serve/InProcess")
    public String operatorServeProcess(ModelMap model) {
        model.addAttribute("active", "serve");
        return "operator/serve/InProcess";
    }           
            
}
