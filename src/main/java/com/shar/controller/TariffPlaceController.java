package com.shar.controller;

import com.shar.service.TariffPlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TariffPlaceController {

    @Autowired
    TariffPlaceService tariffPlaceService;
    
    @RequestMapping("/my")
    public String listRegions(ModelMap model) {
        //model.addAttribute("zones", zoneService.listZones());
        model.addAttribute("tariffplaces", tariffPlaceService.getActiveTariffPlace());
        return "my";
    }
}