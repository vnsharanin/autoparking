package com.shar.controller.menu;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MenuController {

    

    @RequestMapping("/user/configurations/main")
    public String userConfigurations(ModelMap model) {
        model.addAttribute("active", "configurations");
        return "user/configurations";
    }
    @RequestMapping("/user/statistics/main")
    public String userStatistics(ModelMap model) {
        model.addAttribute("active", "statistics");
        return "user/statistics";
    }

    
    
    
    @RequestMapping("/admin/services/main")
    public String adminServices(ModelMap model) {
        model.addAttribute("active", "services");
        return "admin/services/main";
    }

    @RequestMapping("/admin/configurations/main")
    public String adminConfigurations(ModelMap model) {
        model.addAttribute("active", "configurations");
        return "admin/configurations";
    }
    @RequestMapping("/admin/serve/main")
    public String adminServe(ModelMap model) {
        model.addAttribute("active", "serve");
        return "admin/serve";
    }


    
    @RequestMapping("/operator/serve")
    public String operatorServe(ModelMap model) {
        model.addAttribute("active", "serve");
        return "operator/serve";
    }    
}
