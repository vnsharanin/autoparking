function loadList(id, url) {
    $.ajax({
        dataType: "json",
        method: "get",
        url: url,
        success: function (data) {
            addItems(id, data);
        }
    });
}
function setDefault(element) {
    element.empty();
    element.append($("<option>")
            .attr("value", "null")
            .text("None"));
}
function addItems(list, arr) {
    var el = $("#" + list);
    setDefault(el);
    arr.map(function (x) {
        el.append($("<option>")
                .attr("value", x.id)
                .text(x.name));
    });
}
function countryChange(value) {
    if ($("#country option:selected").text() != "None") {
        loadList('region', 'region?country=' + value);
        $("#regionId").show();
    }
    else
    {
        setDefault($("#region"));
        $("#regionId").hide();
    }
    setDefault($("#subRegion"));
    $("#subRegionId").hide();
}
function regionChange(value) {
    if ($("#region option:selected").text() != "None") {
        loadList('subRegion', 'subRegion?region=' + value);
        $("#subRegionId").show();
    }
    else {
        setDefault($("#subRegion"));
        $("#subRegionId").hide();
    }
}
