    function showPassword() {//Will do it with JQuery!
        var password = document.getElementById("password");
        if (password.getAttribute("type") == "password") {
            password.setAttribute('type', 'input');
			$('#showerPassword').html('Скрыть');
        } else {
            password.setAttribute('type', 'password');
			$('#showerPassword').html('Показать');
        }
    }
upp = new Array('','A','B','C','D','E','F','G','H','I','J','K','L',
    'M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
low = new Array('','a','b','c','d','e','f','g','h','i','j','k','l',
    'm','n','o','p','q','r','s','t','u','v','w','x','y','z');
dig = new Array('','0','1','2','3','4','5','6','7','8','9');

function rnd(x,y,z) {
 var num;
 do {
    num = parseInt(Math.random()*z);
    if (num >= x && num <= y) break;
 } while (true);
return(num);
}

function gen_pass() {
var pswrd = '';
var znak, s;
var k = 0;
var n = 8;
var pass = new Array();
var w = rnd(30,80,100);
for (var r = 0; r < w; r++) {
    znak = rnd(1,26,100); pass[k] = upp[znak]; k++;
    znak = rnd(1,26,100); pass[k] = low[znak]; k++;
    znak = rnd(1,10,100); pass[k] = dig[znak]; k++;
}
for (var i = 0; i < n; i++) {
    s = rnd(1,k-1,100);
    pswrd += pass[s];
}
return pswrd;
}