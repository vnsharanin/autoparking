<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
		<title>Services</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript">
            function getData(){
				alert("Login: +7(929)028-3158\nPassword: 7110eda4d09e062aa5e4a390b0a572ac0d2c0220");
			}

        </script>		
    </head>
    <body>
        <div id="verticalBackground">
            <div id="verticalMenu">
                <ul>
                    <li><a href="#"><span>Обслужить по RFID</span></a></li>
					<li><a href="#" class="current"><span>Незакрытые on'line заказы</span></a></li>
					<li><a href="#"><span>Еще необработанные on'line заказы</span></a></li>
                    <li><a href="#"><span>Изменить ТС</span></a></li>
                </ul>
            </div>
        </div>
        <jsp:include flush="true" page="/views/menu/top.jsp"/>    
        <div id="mainDiv" >
            <H2>ОЖИДАЮЩИЕ ПОДГОТОВКИ RFID</H2>
            <br/>
            <table class="services">
                <thead>
                <th>Пользователь</th>
                <th>Статус</th>
				<th>Описание</th>
				<th></th>   				
                </thead>
                <tbody>
                        <tr>
                            <td><a href="#" onclick='javascript:getData();'><span>+7(929)028-3158</span></a></td>
                            <td>В обработке</td>
                            <td>Ожидает подготовки RFID метки</td>
							<td><a href="/user/services/abonement/connect?id=${tariff_abonement.price}"><span>ПЕРЕВЕСТИ В СЛЕДУЮЩИЙ СТАТУС</span></a></td>
                        </tr>						
                </tbody>
            </table><br/><br/>
			<H2>ОЖИДАЮЩИЕ ВРУЧЕНИЯ</H2> <br/>
            <table class="services">
                <thead>
                <th>Пользователь</th>
				<th>Номер карты</th>
                <th>Статус</th>
				<th>Описание</th>
				<th></th>   				
                </thead>
                <tbody>
                        <tr>
                            <td>+7(929)028-3258</td>
							<td>ISG-112-2012-2016</td>
                            <td>В завершении</td>
                            <td>Ожидает вручения пользователю</td>
							<td><a href="/user/services/abonement/connect?id=${tariff_abonement.price}"><span>ПЕРЕВЕСТИ В СЛЕДУЮЩИЙ СТАТУС</span></a></td>
                        </tr>						
                </tbody>
            </table>			
			
        </div>		
    </body>
</html>
