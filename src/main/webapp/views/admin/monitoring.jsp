<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
		<title>Monitoring</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		 
        <script type="text/javascript">
            loadOther(id){
				document.getElementByClassName('current').className='';
				document.getElementById(id).className='current';
				$.ajax({
					dataType: "json",
					method: "get",
					url: "/admin/monitoring/tariffs",
					success: function (data) {
						addItems(id, data); ='<jsp:include flush="true" page="/views/admin/partical/tariffs.jsp"/> ';
					}
				});				
			}
        </script>		
    </head>
    <body>
        <div id="verticalBackground">
            <div id="verticalMenu">
                <ul>
                    <li><a id="other" href="#" class="current"><span>...</span></a></li>        
                    <li><a href="#"><span>Управлять местами</span></a></li>
                    <li><a id="tariffs" href="#" onclick="javascript:loadOther(this.id);"><span>Управлять тарифом</span></a></li>
					<li><a href="#"><span>Параметры посещения</span></a></li>
                </ul>
            </div>
        </div>
        <jsp:include flush="true" page="/views/menu/top.jsp"/>    
        <div id="mainDiv" >
            <span id="body"> </span>
        </div>		
    </body>
</html>
