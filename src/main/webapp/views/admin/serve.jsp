<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
		<title>Services</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div id="verticalBackground">
            <div id="verticalMenu">
                <ul>
                    <li><a href="#" class="current"><span>...</span></a></li>        
                    <li><a href="#"><span>Подтвердить ТС</span></a></li>
					<li><a href="#"><span>Пополнить баланс</span></a></li>
                    <li><a href="#"><span>Изменить ТС</span></a></li>
					<li><a href="#"><span>Заказы RFID</span></a></li>
                </ul>
            </div>
        </div>
        <jsp:include flush="true" page="/views/menu/top.jsp"/>    
        <div id="mainDiv" >
            Hello
        </div>		
    </body>
</html>
