<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; UTF-8">
        <title>HelloWorld Login page</title>
        <script type="text/javascript" src="resources/scripts/password.js"></script>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>	

        <script src="resources/js/modernizr.js"></script> <!-- Modernizr -->
        <script src="/resources/js/main.js"></script> <!-- Gem jQuery -->
        <script src="/resources/scripts/agree.js" type="text/javascript"></script>
        <script src="/resources/scripts/jquery.maskedinput.js" type="text/javascript"></script>
        <script type="text/javascript">
            jQuery(function ($) {
                $("#username").mask("+7 (999) 999-9999");
                $("#usernameRegister").mask("+7 (999) 999-9999");
                $("#usernameHelp").mask("+7 (999) 999-9999");
            });

        </script>
    </head>
    <body>
        <sec:authorize access="!isAuthenticated()">
            <div class="cd-user-modal"> <!-- все формы на фоне затемнения-->
                <div class="cd-user-modal-container"> <!-- основной контейнер -->
                    <ul class="cd-switcher">
                        <li><a href="#0" class="bodyText">Вход</a></li>
                        <li><a href="#0" class="bodyText">Регистрация</a></li>
                    </ul>

                    <div id="cd-login"> <!-- форма входа -->
                        <c:url var="loginUrl" value="/login" />
                        <form:form action="${loginUrl}" method="post" class="cd-form">
                            <p class="fieldset">
                                <input class="full-width has-padding has-border" id="username" name="telephone" type="text" placeholder="Номер телефона" required>
                            </p>
                            <p class="fieldset"> 
                                <input class="full-width has-padding has-border" id="password" name="password" type="password"  placeholder="Пароль" required>
                                <a href="#0" id="showerPassword" class="hide-password" onClick="showPassword()">Показать</a>
                            </p>
                            <p class="fieldset">
                                <input type="checkbox" id="rememberme" name="remember-me">
                                <label for="rememberme">Запомнить меня</label>
                            </p>
                            <p class="fieldset">
                                <input class="full-width" type="submit" value="Войти">
                            </p>
                        </form:form>

                        <p class="cd-form-bottom-message"><a href="#0">Забыли свой пароль?</a></p>
                    </div>

                    <div id="cd-signup"> <!-- форма регистрации -->
                        <c:url var="loginUrl" value="/register" />
                        <form:form action="${loginUrl}" method="post" class="cd-form">
                            <p class="fieldset">
                                <input class="full-width has-padding has-border" id="usernameRegister" name="telephone" type="text" placeholder="Номер телефона" required>
                            </p>
                            <p class="fieldset">
                                <input type="checkbox" id="accept-terms" onchange="agree()">
                                <label for="accept-terms">Я согласен с <a href="#0">Условиями</a></label>
                            </p>
                            <input type="hidden" id="gp" name="gpassword" value=""/>
                            <p class="fieldset">
                                <input class="full-width has-padding" type="submit" value="Создать аккаунт" id="registerSubmit" name="action" onClick="document.getElementById('gp').value = gen_pass();" disabled="disabled"/>
                            </p>
                        </form:form>
                    </div>

                    <div id="cd-reset-password"> <!-- форма восстановления пароля -->
                        <p class="cd-form-message">Забыли пароль? Пожалуйста, введите номер мобильного телефона, чтобы получить его.</p>

                        <form class="cd-form">
                            <p class="fieldset">
                                <input class="full-width has-padding has-border" id="usernameHelp" name="telephone" type="text" placeholder="Номер телефона" required>
                            </p>
                            <p class="fieldset">
                                <input class="full-width has-padding" type="submit" value="Восстановить пароль">
                            </p>
                        </form>

                        <p class="cd-form-bottom-message"><a href="#0">Вернуться к входу</a></p>
                    </div>
                    <a href="#0" class="cd-close-form">Закрыть</a>
                </div>
            </div>		
        </sec:authorize>	
    </body>
</html>