<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Services</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div id="verticalBackground">
            <div id="verticalMenu">
                <ul>
                    <li><a href="/visitor/services/main" class="current"><span>...</span></a></li>        
                    <li><a href="/visitor/services/reservation"><span>Бронь</span></a></li>
                    <li><a href="/visitor/services/abonement"><span>Абонемент</span></a></li>
                </ul>
            </div>
        </div>
        <jsp:include flush="true" page="/views/menu/top.jsp"/>  	
        <div id="mainDiv">
            <H2>ВНИМАНИЕ!</H2> <br/>
            <H2>ВЫ НАХОДИТЕСЬ В РЕЖИМЕ ПРОСМОТРА ОСНОВНОЙ ИНФОРМАЦИИ!</H2><br/>
            <H2>ДЛЯ ПРОСМОТРА/ИСПОЛЬЗОВАНИЯ ВСЕХ ФУНКЦИОНАЛЬНЫХ ВОЗМОЖНОСТЕЙ НАЖМИТЕ:</H2><br/>
            <a href="#0" class="accountLogIn"><span class="indexButton"><H2>ВХОД/РЕГИСТРАЦИЯ</H2></span></a>
        </div>
    </body>
</html>
