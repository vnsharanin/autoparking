<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html>
<html>
    <head>
        <title>Users</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <script type="text/javascript" src="/resources/scripts/jquery-1.8.2.min.js"></script>
        <script type="text/javascript" src="/resources/scripts/toggleblock.js"></script>
    </head>
    <body>

        <h2>User List</h2>

        <c:if test="${!empty listOfUsers}">
            <table>
                <tr>
                    <th>email</th>
                    <th>telephone</th>
                    <th>roles</th>
                    <th>&nbsp</th>
                    <th>&nbsp</th>
                    <th>&nbsp</th>
                </tr>
                <c:forEach items="${listOfUsers}" var="user">
                    <tr>
                        <td>${user.email}</td>
                        <td>${user.telephone}</td>
                        <td><a href="javascript:roles('${user.id}','roles');" id='${user.id}link' onclick="javascript:loadRoles('${user.id}');">show roles</a>
                            <div id='${user.id}block' style='display:none'>
                                checkboxes (if has role - checkbox must be checked, else empty)
                                <br> Add an allow changing roles in here (ajax)
                            </div>
                        </td>
                        <td><a href="${pageContext.request.contextPath}/admin/user/details/${user.id}">Details</a></td>
                        <td><a href="${pageContext.request.contextPath}/admin/user/edit/${user.id}">Edit</a></td>
                        <td><a href="${pageContext.request.contextPath}/admin/user/delete/${user.id}">Delete</a></td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
    </body>
</html>