<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<table>
    <tr>
        <td><form:input path="telephone" id="telephone" /></td>
        <td><form:errors path="telephone" cssclass="error"></form:errors></td>
        </tr>
        <tr>
            <td><form:password path="password" id="password"/>
            <input type="button" value="Show password" onclick="javascript:showPassword();"/>
        </td>

        <td><form:errors path="password" cssclass="error"></form:errors></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Register"/>
        </td>
    </tr>
</table>