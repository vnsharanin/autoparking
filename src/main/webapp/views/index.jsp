﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Hello!</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

    </head>
    <body>
        <jsp:include flush="true" page="menu/top.jsp"/>
        <div id="mainDiv" >
            <div id="index">
                <H1>Минимальный необходимый набор первых действий к получению всех возможностей!</H1>
                <br/><br/>
                <H1>ШАГ 1</H1>
                <br/>
                <a href="#0" class="accountLogIn"><span class="indexButton"><h2>РЕГИСТРАЦИЯ<br/>(и мы уже автоматически начинаем готовить RFID Метку для Вас)</h2></span></a>
                <br/>
                <a href="#0" class=""><span class="indexButton"><h2>ДОБАВЛЕНИЕ ТС <br/>(и подтвердить ТС Вы сможете сразу же при получении RFID)</h2></span></a>
                <br/><br/>
                <H1>ШАГ 2</H1>
                <br/>
                <a href="#0" class=""><span class="indexButton"><h2>ПОЛУЧЕНИЕ RFID</h2></span></a>
                <br/>
                <a href="#0" class=""><span class="indexButton"><h2>ПОДТВЕРЖДЕНИЕ ТС ПО ПТС<br/>(для каждого добавленного ТС)</h2></span></a>            


            </div>
        </div>
    </body>
</html>
