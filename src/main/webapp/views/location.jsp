<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html>
<html>
    <head>
        <title>Your_location</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="_csrf" content="${_csrf.token}"/>
        <meta name="_csrf_header" content="${_csrf.headerName}"/>

        <script type="text/javascript" src="resources/scripts/jquery-1.8.2.min.js"></script>
        <script type="text/javascript" src="resources/scripts/location.js"></script>
    </head>
    <body>
        <p>Country:
            <select id='country' onchange='javascript:countryChange(this.value);'>
                <option value="null" selected>None</option>
                <c:forEach var = "country" items = "${countries}">
                    <option value=${country.id}>${country.name}</option>
                </c:forEach>
            </select>
        </p>
        <div id="regionId" style="display:none">
            <p>Region:
                <select id='region' onchange='javascript:regionChange(this.value);'>
                    <option value="null" selected>None</option>
                </select>
            </p>
        </div>
        <div id="subRegionId" style="display:none">
            <p>SubRegion:
                <select id='subRegion'>
                    <option value="null" selected>None</option>
                </select>
            </p>
        </div>
    </body>
</html>