<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Services</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <jsp:include flush="true" page="/views/user/vertical.jsp"/> 
        <jsp:include flush="true" page="/views/menu/top.jsp"/>  	
        <div id="mainDiv" >
            <H2>АКТИВНЫЙ ТАРИФ НА БРОНИРОВАНИЕ</H2>
            <br/>
            <table class="services">
                <thead>
                <th>Первое бесплатное время с момента подключения (мин)</th>
                <th>Цена за час по истечении бесплатного времени (руб)</th>
                <th>Время действия с момента подключения подключения (ч)</th>
                </thead>
                <tbody>
                    <tr>
                        <td>${active_tariff_reservation.firstFreeTimeInMinutes}</td>
                        <td>${active_tariff_reservation.priceInHourAfterFreeTime}</td>
                        <td>${active_tariff_reservation.timeOfApplicationInHours}</td>
                    </tr>
                </tbody>
            </table>
            <br/>
            <a href="#0" class="accountLogIn"><span class="reservationButton"><H2>ЗАБРОНИРОВАТЬ МЕСТО</H2></span></a>            
        </div>
    </body>
</html>
