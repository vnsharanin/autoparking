<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Services</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
		<jsp:include flush="true" page="/views/user/vertical.jsp"/>  
        <jsp:include flush="true" page="/views/menu/top.jsp"/>  	
        <div id="mainDiv">
            <table class="tariff_places">
                <tbody>
                        <tr>
                            <td class="mainColumn">БРОНЬ</td>
                            <td>УСЛУГА ДОСТУПНА</td>
                        </tr>                          
                        <tr>
                            <td class="mainColumn">АБОНЕМЕНТ</td>
                            <td>УСЛУГА ДОСТУПНА</td>
                        </tr>
                        <tr>
                            <td class="mainColumn">ПОПОЛНИТЬ БАЛАНС</td>
                            <td>УСЛУГА ДОСТУПНА</td>
                        </tr>
                        <tr>
                            <td class="mainColumn">ЗАКАЗАТЬ RFID</td>
                            <td>УСЛУГА ДОСТУПНА</td>
                        </tr>  						
                </tbody>
            </table>
        </div>
    </body>
</html>
