<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Services</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <jsp:include flush="true" page="/views/user/vertical.jsp"/>  
        <jsp:include flush="true" page="/views/menu/top.jsp"/>  	
        <div id="mainDiv" >
            <H2>АКТИВНЫЕ АБОНЕМЕНТЫ</H2>
            <br/>
            <table class="services">
                <thead>
                <th>Наименование</th>
                <th>Количество дней</th>
                <th>Количество посещений</th>
                <th>Включает ли в себя постоянную бронь места</th>
                <th>Цена</th>
				<th></th>   				
                </thead>
                <tbody>
                    <c:forEach var = "tariff_abonement" items = "${tariffs_abonement}">
                        <tr>
                            <td>${tariff_abonement.name}</td>
                            <td>${tariff_abonement.amountDays}</td>
                            <td>${tariff_abonement.amountVisits}</td>
                            <td>${tariff_abonement.reservation}</td>
                            <td>${tariff_abonement.price}</td>
                            <td><a href="/user/services/abonement/connect?id=${tariff_abonement.id}"><span>ПОДКЛЮЧИТЬ</span></a></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
