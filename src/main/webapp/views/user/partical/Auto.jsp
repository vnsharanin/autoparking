<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table border="1">
            <c:forEach var = "zone" items = "${zones}">
                <tr>
                    <td>${zone.id}</td>
                    <td>${zone.name}</td>
                    <td>${zone.description}</td>
                </tr>
            </c:forEach>
        </table>
        <br>
        <table border="1">
            <c:forEach var = "tariffPlace" items = "${tariffplaces}">
                <tr>
                    <td>${tariffPlace.id}</td>
                    <td>${tariffPlace.type}</td>
                    <td>${tariffPlace.climateControl}</td>
                    <td>${tariffPlace.priceInHourWithoutAbonement}</td>
                    <td>${tariffPlace.priceInHourWithAbonement}</td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
