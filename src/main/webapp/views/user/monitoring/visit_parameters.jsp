<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Monitoring</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div id="verticalBackground">
            <div id="verticalMenu">
                <ul>
                    <li><a id="other" href="/admin/monitoring/other" class="current"><span>...</span></a></li>        
                    <li><a href="/admin/monitoring/places"><span>Управлять местами</span></a></li>
                    <li><a id="tariffs" href="/admin/monitoring/tariffs"><span>Управлять тарифом</span></a></li>
                    <li><a href="/admin/monitoring/visit_parameters"><span>Параметры посещения</span></a></li>
                </ul>
            </div>
        </div>
        <jsp:include flush="true" page="/views/menu/top.jsp"/>  	
        <div id="mainDiv" >

        </div>
    </body>
</html>
