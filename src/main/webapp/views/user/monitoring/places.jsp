<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Monitoring</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div id="verticalBackground">
            <div id="verticalMenu">
                <ul>
                    <li><a id="other" href="/admin/monitoring/other" ><span>...</span></a></li>        
                    <li><a href="/admin/monitoring/places"><span>Управлять местами</span></a></li>
                    <li><a id="tariffs" href="/admin/monitoring/tariffs" class="current"><span>Управлять тарифом</span></a></li>
                    <li><a href="/admin/monitoring/visit_parameters"><span>Параметры посещения</span></a></li>
                </ul>
            </div>
        </div>
        <jsp:include flush="true" page="/views/menu/top.jsp"/>  	
        <div id="mainDiv" >
            <table align="center">
                <thead>
                    <th>ID</th>
                    <th>Type</th>
                    <th>climateControl</th>
                    <th>priceInHourWithoutAbonement</th>
                    <th>priceInHourWithAbonement</th>
                </thead>
                <tbody>
                    <c:forEach var = "tariffPlace" items = "${tariffplaces}">
                        <tr>
                            <td>${tariffPlace.id}</td>
                            <td>${tariffPlace.type}</td>
                            <td>${tariffPlace.climateControl}</td>
                            <td>${tariffPlace.priceInHourWithoutAbonement}</td>
                            <td>${tariffPlace.priceInHourWithAbonement}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
