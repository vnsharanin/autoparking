<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Monitoring</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div id="verticalBackground">
            <div id="verticalMenu">
                <ul>
                    <li><a id="main" href="/user/monitoring/main" class="current"><span>...</span></a></li>
                    <li><a id="contacts" href="/user/monitoring/contacts"><span>Контакты</span></a></li>
                </ul>
            </div>
        </div>
        <jsp:include flush="true" page="/views/menu/top.jsp"/>  	
        <div id="mainDiv" >
           
            
            <H2>НАГРУЖЕННОСТЬ ПАРКОВКИ</H2><br/>
             <H2>ВСЕГО МЕСТ: 250</H2><br/>
            <table class="tariff_places">
                <tbody>
                        <tr>
                            <td class="mainColumn">СВОБОДНЫХ МЕСТ</td>
                            <td>250</td>
                        </tr>                          
                        <tr>
                            <td class="mainColumn">МЕСТ, ОЖИДАЮЩИХ ПОСЕЩЕНИЯ</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td class="mainColumn">ЗАНЯТЫХ МЕСТ</td>
                            <td>0</td>
                        </tr>                       
                </tbody>
            </table>
            
            <br/><br/>
            <H2>ТАРИФ ПАРКОВКИ</H2><br/>
            <table class="tariff_places">
                <thead>
                <th>Тип парковочной зоны</th>
                <th>Наличие климат-контроля</th>
                <th>Цена за час без абонемента</th>
                <th>Цена за час с абонементом</th>
                </thead>
                <tbody>
                    <c:forEach var = "tariffPlace" items = "${tariff_place}">
                        <tr>
                            <td>${tariffPlace.type}</td>
                            <td>${tariffPlace.climateControl}</td>
                            <td>${tariffPlace.priceInHourWithoutAbonement}</td>
                            <td>${tariffPlace.priceInHourWithAbonement}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <br/><br/>
            <H2>ПАРАМЕТРЫ ПОСЕЩЕНИЯ ПАРКОВКИ</H2><br/>
            <table class="visit_parameters">
                <thead>
                <th>Первое бесплатное время после совершения въезда</th>
                <th>Первое бесплатное время для погашения задолженности</th>
                </thead>
                <tbody>
                    <c:forEach var = "visit_parameters" items = "${visits_parameters}">
                        <tr>
                            <td>${visit_parameters.firstFreeTimeInMinutes}</td>
                            <td>${visit_parameters.firstFreeTimeOnCashing}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>			
        </div>
    </body>
</html>
