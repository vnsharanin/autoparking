<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <title>Register</title>
        <script type="text/javascript" src="resources/scripts/password.js"></script>
    </head>
    <body>
        <div>
            <h2>User Registration Form</h2>
            <form:form method="post"
                       action="${pageContext.request.contextPath}/register/new"
                       commandName="user">
                <jsp:include flush="true" page="user_form.jsp">
                    <jsp:param name="adduser" value="true" />
                </jsp:include>
            </form:form>
        </div>
    </body>
</html>