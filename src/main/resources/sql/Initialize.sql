insert into Tariffs_Places(climate_control,type,status,price_in_hour_without_abonement,price_in_hour_with_abonement)
values ('NO','Closed','Active',0,0),
('YES','Closed','Disable',0,0),
('NO','Opened','Disable',0,0);

insert into Tariffs_Reservation(first_free_time_in_minutes,price_in_hour_after_free_time,status,time_of_application_in_hours)
values (0,0,'Active',0);

insert into Tariffs_Abonements(name, amount_days, amount_visits,price)
values ('initialize',0,0,0);
										
insert into Visit_Parameters(first_free_time_in_minutes,first_free_time_on_cashing,Status)
values (0,0,'Active');