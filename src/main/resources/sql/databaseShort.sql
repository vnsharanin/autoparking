﻿drop database parking;
create database parking;
use parking;	
	
CREATE TABLE persistent_logins (
    username VARCHAR(64) NOT NULL,
    series VARCHAR(64) NOT NULL,
    token VARCHAR(64) NOT NULL,
    last_used TIMESTAMP NOT NULL,
    PRIMARY KEY (series)
);

CREATE TABLE Configurations
(
	id BIGINT not null auto_increment,
	isChainToPlace BIGINT null,
	searchPlaceAlgorithm BIGINT not null,
	isDefault BIGINT null,
	PRIMARY KEY(id)
);

insert into Configurations(isChainToPlace,searchPlaceAlgorithm,isDefault)
values (1,0,1);

create table Users (
   id BIGINT NOT NULL AUTO_INCREMENT,
   email VARCHAR(30) NULL,
   telephone VARCHAR(20) NOT NULL,

   password VARCHAR(256) NOT NULL,
   status VARCHAR(30) NOT NULL, 
   configuration BIGINT not null default 1,
   PRIMARY KEY (id),
   FOREIGN KEY(configuration) REFERENCES Configurations(id),
   UNIQUE (telephone)
);
  
create table Roles(
   id BIGINT NOT NULL AUTO_INCREMENT,
   name VARCHAR(30) NOT NULL,
   PRIMARY KEY (id),
   UNIQUE (name)
);
  
CREATE TABLE Users_Roles (
	id BIGINT NOT NULL AUTO_INCREMENT,
    user BIGINT NOT NULL,
    role BIGINT NOT NULL,
    PRIMARY KEY (id),
	UNIQUE (user, role),
    CONSTRAINT FK_Users FOREIGN KEY (user) REFERENCES Users (id),
    CONSTRAINT FK_Roles FOREIGN KEY (role) REFERENCES Roles (id)
);
 
INSERT INTO Roles(name)
VALUES ('USER'),('ADMIN'),('DBA');


INSERT INTO Users(email, telephone, password, status)
VALUES ('shar@mail.ru','+7(920)902-8546','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Active'), ('shar2@mail.ru','+7(929)028-3258','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Active');

INSERT INTO Users_Roles (user, role)
  SELECT user.id, role.id FROM Users user, Roles role
  where user.email='shar@mail.ru' and role.name='ADMIN';
INSERT INTO Users_Roles (user, role)
  VALUES (1,1), (2,1);
  
CREATE TABLE Balance_Operations
(
	id BIGINT NOT NULL AUTO_INCREMENT,
	user BIGINT not null,
	type_operation char(255) not null,
	sum INT not null,
	current_balance INT not null,
	description char(255) null,
	date_operation char(255) not null,
	status char(255) not null default 'True',
	FOREIGN KEY(user) REFERENCES Users(id),
	PRIMARY KEY (id)
);

CREATE TABLE Autos
(
	id BIGINT not null auto_increment,
	number char(10) not null,
	scanner_as char(255) null,
	PRIMARY KEY(id)
);

CREATE TABLE Users_Autos
(
	id BIGINT not null auto_increment,
	auto BIGINT not null,
	user BIGINT not null,
	document char(255) null,
	status BOOLEAN not null,
	PRIMARY KEY(id),
	FOREIGN KEY(auto) REFERENCES Autos(id),
	FOREIGN KEY(user) REFERENCES Users(id)
);

CREATE TABLE Queue_on_service
(
	id BIGINT not null auto_increment,
	type char(255) null,
	status char(255) null,
	operator BIGINT null,
	user BIGINT not null,
	PRIMARY KEY(id),
	FOREIGN KEY(user) REFERENCES Users(id)
);

CREATE TABLE Tariffs_Places
(
	id BIGINT not null auto_increment,
	climate_control char(3) not null default '',
	type char(6) not null default '',
	status char(255) not null default 'Active',
	price_in_hour_without_abonement INT not null default 0,
	price_in_hour_with_abonement INT not null default 0,
	PRIMARY KEY(id)
);

INSERT INTO Tariffs_Places (climate_control,type,price_in_hour_without_abonement,price_in_hour_with_abonement)
values ('NO','Closed',12,0),
('NO','Opened',10,0),
('YES','Closed',15,0);

CREATE TABLE Places
(
	id BIGINT not null auto_increment,
	number INT not null,
	status char(255) not null default 'Free',
	next_status char(255) null,
	replacing_place BIGINT null,
	tariff_on_place BIGINT not null,
	FOREIGN KEY(tariff_on_place) REFERENCES tariffs_places(id),
	PRIMARY KEY(id)
);

CREATE TABLE Tariffs_Reservation
(
	id BIGINT not null auto_increment,
	first_free_time_in_minutes int(11) not null default 0,
	price_in_hour_after_free_time INT not null,
	status char(255) not null default 'available',
	time_of_application_in_hours INT not null default 12,
	Primary Key(id)
);

CREATE TABLE Users_Reservations
(
	id BIGINT not null auto_increment,
	tariff_reservation BIGINT not null,
	user BIGINT not null,
	date_connection char(255) null,
	date_out_from_activity char(255) null,
	place BIGINT null,
	alternative_location_place BIGINT null,
	status char(255) not null default 'Formed',
	FOREIGN KEY(tariff_reservation) REFERENCES Tariffs_Reservation(id),
	FOREIGN KEY(user) REFERENCES Users(id),
	FOREIGN KEY(place) REFERENCES Places(id),
	Primary Key(id)
);

CREATE TABLE Tariffs_Abonements
(
	id BIGINT not null auto_increment,
	name VARCHAR(50) not null,
	amount_days int(11) null default 0,
	amount_visits int(11) null default 0,
	price INT not null default 0,
	reservation BOOLEAN not null default 0,
	status char(25) not null default 'available',
	PRIMARY KEY(id)
);

CREATE TABLE Users_Abonements
(
	id BIGINT not null auto_increment,
	abonement BIGINT not null,
	connection_date char(255) not null,
	status char(20) not null default 'Active',
	amount_completed_visits int(11) not null default 0,
	user BIGINT not null,
	date_out_from_activity char(255) null,
	place BIGINT null,
	FOREIGN KEY(place) REFERENCES Places(id),
	FOREIGN KEY(abonement) REFERENCES Tariffs_Abonements(id),
	FOREIGN KEY(user) REFERENCES Users(id),
	PRIMARY KEY(id)
);


CREATE TABLE Visit_Parameters
(
	id BIGINT not null auto_increment,
	first_free_time_in_minutes int(11) not null default 0,
	first_free_time_on_cashing int(11) not null default 0,
	Status char(255) not null default 'Active',
	PRIMARY KEY(id)
);


CREATE TABLE Visits
(
	id BIGINT not null auto_increment,
	user_auto BIGINT not null,	
	place BIGINT not null,
	visit_parameters BIGINT not null,
	date_in char(255) not null default '00-00-00 00:00:00',
	date_out char(255) null default 'dateout',
	first_attempt_go_out char(255) null default 'dateout2',
	next_attempt_go_out char(255) null default 'dateout3',
	
	Status char(255) not null default 'Public',
	PRIMARY KEY(id),
	FOREIGN KEY(user_auto) REFERENCES Users_Autos(id),
	FOREIGN KEY(place) REFERENCES Places(id),
	FOREIGN KEY(visit_parameters) REFERENCES Visit_Parameters(id)
);
